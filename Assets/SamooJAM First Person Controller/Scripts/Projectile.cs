﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	public float timeoutDestructor = 5;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		timeoutDestructor -= Time.deltaTime;
		if (timeoutDestructor <= 0f) {
			gameObject.SetActive (false);
		}
	}
}
