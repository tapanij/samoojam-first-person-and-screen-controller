﻿@script RequireComponent(AudioSource)

public var leftStep: AudioSource;
public var rightStep: AudioSource;
private var currentStep = 0;
private var waitTime = 0.7;

function Start () {
  // create an infinite loop that runs every frame:
  while (true){
    if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1 || Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1){
          if(currentStep == 0){
          leftStep.Play();
          currentStep = 1;
          }
          else {
          rightStep.Play();
            currentStep = 0;
          }
        yield WaitForSeconds(waitTime);

    }
    yield; // let Unity free till next frame
  }
}