﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
	public Transform itemCurrentlyHeld;
	private bool itemUsesGravity = false;
	public Transform crossHair;
	private bool usable = false;
	private GUIStyle style;
	public Font font;
	public Projectile projectilePrefab;
	private const int maxProjectiles = 50;
	private Projectile[] projectiles;
	private int projectileIndex = 0;
	public GameObject gun;

	void Awake ()
	{
		style = new GUIStyle ();
		style.font = font; 
		style.normal.textColor = Color.cyan;

		// Projectiles
		projectiles = new Projectile[maxProjectiles];
		for (int i = 0; i < maxProjectiles; i++) {
			projectiles [i] = (Projectile)Instantiate (projectilePrefab, Vector3.zero, Quaternion.identity);
			projectiles [i].gameObject.SetActive (false);
		}
	}

	void Update ()
	{
		// Bit shift the index of the layer (8) to get a bit mask
		LayerMask layerMask = 1 << 8;
		// This would cast rays only against colliders in layer 8.
		// But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
		layerMask = ~layerMask;
  
		if (itemCurrentlyHeld) {
			itemCurrentlyHeld.rigidbody.MovePosition (Vector3.Lerp (itemCurrentlyHeld.rigidbody.position, crossHair.position, 4f * Time.deltaTime));// * speed * Time.deltaTime);
			Vector3 eulerAngleVelocity = new Vector3 (1, 1, 1);
			Quaternion deltaRotation = Quaternion.Euler (eulerAngleVelocity * Time.deltaTime);
			itemCurrentlyHeld.rigidbody.MoveRotation (transform.rotation * deltaRotation);
		}

		if (Input.GetButtonDown ("Fire1") && gun.activeSelf) {
			// SHOOT PROJECTILE
			projectiles [projectileIndex].gameObject.SetActive (true);
			projectiles [projectileIndex].timeoutDestructor = 5;
			Vector3 startPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			startPosition += transform.TransformDirection (Vector3.forward * 1.5f);
			startPosition += transform.TransformDirection (Vector3.right * 0.2f);
			startPosition += transform.TransformDirection (Vector3.down * 0.34f);
			projectiles [projectileIndex].rigidbody.velocity = transform.TransformDirection (Vector3.forward * 50f);
			projectiles [projectileIndex].rigidbody.angularVelocity = Vector3.zero;
			projectiles [projectileIndex].transform.position = startPosition;
			projectiles [projectileIndex].transform.rotation = transform.rotation;
			projectiles [projectileIndex].rigidbody.useGravity = true;
			
			if (projectileIndex + 1 == maxProjectiles) {
				projectileIndex = 0;
			} else {
				projectileIndex++;	
			}

			// Audio
			gun.audio.Play();
		}

		RaycastHit hit;

		Vector3 direction = crossHair.position - transform.position;
		if (Input.GetButtonDown ("Fire2") && itemCurrentlyHeld) {
			if (itemCurrentlyHeld.rigidbody) {
				itemCurrentlyHeld.rigidbody.velocity = Vector3.zero;
				itemCurrentlyHeld.rigidbody.angularVelocity = Vector3.zero;
			}
			itemCurrentlyHeld.rigidbody.useGravity = itemUsesGravity;
			itemCurrentlyHeld.gameObject.layer = 0;
			itemCurrentlyHeld = null;
	
		} else if (Physics.Raycast (transform.position, direction, out hit, 2.6f, layerMask)) {

			if (hit.collider.CompareTag ("carryable")) {
				usable = true;
				if (Input.GetButtonDown ("Fire2") && !itemCurrentlyHeld) {
					itemCurrentlyHeld = hit.collider.transform;
					itemUsesGravity = itemCurrentlyHeld.rigidbody.useGravity;
					itemCurrentlyHeld.rigidbody.useGravity = false;
					itemCurrentlyHeld.gameObject.layer = 9;
				}
			} 
			else if (usable){
				usable = false;
			}

			if (Input.GetButtonDown ("Fire2")) {

			} 	
		} else if (usable){
			usable = false;
		}
	}

	void OnGUI ()
	{
		if (usable)
			GUI.Label (new Rect (Screen.width * 0.49f, Screen.height * 0.45f, 100, 20), "Use(mouse2)", style);
	}
}
