private var xtargetRotation:float=10;
private var ytargetRotation:float=10;
var xSensitivity:float=10;
var ySensitivity:float=10;
var smoothing=1;
var min=-60;
var max=60;
var minX=-90;
var maxX=90;
public var inVehicle = false;

function Update () 
{
// Change sensitivy
if (xSensitivity > 0.5f && (Input.GetKeyUp(KeyCode.Minus) || Input.GetKeyUp(KeyCode.KeypadMinus) ))
{
	xSensitivity -=0.5f; 
	ySensitivity -=0.5f;
}

if (xSensitivity < 50f && (Input.GetKeyUp(KeyCode.Plus) || Input.GetKeyUp(KeyCode.KeypadPlus) || Input.GetKeyUp(KeyCode.Equals) ) )
{
	xSensitivity +=0.5f; 
	ySensitivity +=0.5f;
}

if (Input.GetKeyUp(KeyCode.M))
{
	if(smoothing == 0)
		smoothing = 1;
	else
		smoothing = 0;
}


var yAxisMove:float=Input.GetAxis("Mouse Y")*ySensitivity; // how much has the mouse moved?
ytargetRotation+=-yAxisMove; // what is the target angle of rotation
ytargetRotation=ytargetRotation % 360;
ytargetRotation=Mathf.Clamp(ytargetRotation,min,max);

var xAxisMove:float=Input.GetAxis("Mouse X")*xSensitivity; // how much has the mouse moved?
xtargetRotation+=xAxisMove; // what is the target angle of rotation
xtargetRotation=xtargetRotation % 360;
if(inVehicle){
	xtargetRotation=Mathf.Clamp(xtargetRotation,minX,maxX);
}


if(transform.parent.gameObject.tag == "vehicle"){
	transform.localRotation=Quaternion.Lerp(transform.localRotation,Quaternion.Euler(ytargetRotation,xtargetRotation,0),Time.deltaTime*10/smoothing);
}
else{
	transform.localRotation=Quaternion.Lerp(transform.localRotation,Quaternion.Euler(ytargetRotation,0,0),Time.deltaTime*10/smoothing);
	transform.parent.rotation=Quaternion.Lerp(transform.parent.rotation,Quaternion.Euler(0,xtargetRotation,0),Time.deltaTime*10/smoothing);
}
}