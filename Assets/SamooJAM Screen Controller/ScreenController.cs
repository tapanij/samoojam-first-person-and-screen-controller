﻿using UnityEngine;
using System.Collections;

public class ScreenController : MonoBehaviour
{
	void Start ()
	{
		
	}
	
	void DidLockCursor ()
	{
		Debug.Log ("Locking cursor");
	}
	
	void DidUnlockCursor ()
	{
		Debug.Log ("Unlocking cursor");
	}
	
	void OnMouseDown ()
	{
		Screen.lockCursor = true;
	}
	
	private bool wasLocked = false;
	
	void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) {
			OnMouseDown ();
		}
		
		if (Input.GetKeyDown ("escape"))
			Screen.lockCursor = false;
		
		if (!Screen.lockCursor && wasLocked) {
			wasLocked = false;
			DidUnlockCursor ();
		} else if (Screen.lockCursor && !wasLocked) {
			wasLocked = true;
			DidLockCursor ();
		}
		
		/** Fullsreen switch **/
		if (Input.GetKeyDown(KeyCode.F)) {
			Debug.Log ("fullscreen");
			Screen.fullScreen = !Screen.fullScreen;
			
			
			if (Screen.fullScreen) {
				// Set your windowed resolution to whatever you want here.
				Screen.SetResolution (1152, 648, false);
			} else {
				// Switch to the desktop resolution in fullscreen mode.
				Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
			}
		}
	}
}
